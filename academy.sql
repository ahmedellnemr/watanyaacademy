-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2021 at 04:37 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `academy`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `activity_department_id` bigint(20) UNSIGNED DEFAULT NULL,
  `main_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contents` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `activity_department_id`, `main_image`, `title`, `contents`, `created_at`, `updated_at`) VALUES
(1, 1, 'activitys/Ywn2dAA7HAPqtaxfNEuMFaFYd1wXSL0MGafTNVdh.jpeg', 'نشاط الاكاديمية 1', 'لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم مطبوعه ... بروشور او فلاير على سبيل المثال ... او نماذج مواقع انترنت', '2021-04-02 22:27:41', '2021-04-13 22:45:29'),
(6, 2, 'activitys/6JVeEWLRTEOC5YXw9knpEN7uDuhlwfaHvAz50auN.jpeg', 'نشاط 2', 'سوف يقام مؤتمر القمة العالمية بالقاهرة الاسبوع المقبل وذلك بحضور قامة من العلماء والرؤساء وكبار رجال الدولة', '2021-04-03 00:39:06', '2021-04-13 22:41:54');

-- --------------------------------------------------------

--
-- Table structure for table `activity_departments`
--

CREATE TABLE `activity_departments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contents` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ليس ضرورى',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_departments`
--

INSERT INTO `activity_departments` (`id`, `title`, `contents`, `created_at`, `updated_at`) VALUES
(1, 'ندوات', NULL, '2021-04-02 22:17:09', '2021-04-02 22:18:20'),
(2, 'مؤتمرات', NULL, '2021-04-02 22:17:18', '2021-04-02 22:18:13');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `admin_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `image`, `group_id`, `admin_type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$1VE/l.naiKV/Opgu6xl79efaNTNXyKTfxqhnEcaZTrxUTigYKinsy', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'admin2', 'admin2@admin2.com', '$2y$10$xRTie3BfIn3onzwj0A37YuLsWvYiJ73dQ7DgLB161j9fDup2D8Jc6', NULL, NULL, NULL, NULL, '2021-03-29 21:23:19', '2021-03-29 21:23:19');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key_word` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contents` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `key_word`, `url_name`, `title`, `contents`, `background`, `video`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'أهلا بك فى الأكاديمية الوطنية للعلوم الشاملة', 'بتسجيلك فى برنامج الأكاديمية الوطنية للعلوم الشاملة تتجاوز العقبات وتتعلم العلم بكل يسر وسهولة من خلال الوسائل المتقدمة المعتمدة فى برنامج الأكاديمية ...', 'banners/fQ9lRDCIg6233uM18T5jt2AwUVOvzwONlv04F8ki.jpeg', 'banners/tPJNCunDpQJIcBW2yMYIlZAFOLGZvyyVp3YzExAK.mp4', NULL, '2021-04-10 20:08:33'),
(2, 'vision', NULL, 'صفحة الرؤية والاهداف', 'و هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم', 'banners/DRqn1yd71K9BJXvvR95GaBD8lNyACwZQNi98tBSm.jpeg', 'banners/tPJNCunDpQJIcBW2yMYIlZAFOLGZvyyVp3YzExAK.mp4', NULL, '2021-04-03 13:42:35'),
(6, 'word_of_prestent', '#', 'صفحة رئيس الاكاديمية', 'و هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم', 'banners/TmayHG7eKJr3VziQlQTQJEJoWzZ7QhvFb2AZ07X0.jpeg', NULL, '2021-04-03 13:45:16', '2021-04-03 13:45:16'),
(7, 'lessons', '#', 'الكورسات', 'و هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم', 'banners/gLgpOmSWZNwMebX0MlgsSaxwhSNuiLdT2YjafkCU.jpeg', NULL, '2021-04-03 13:45:52', '2021-04-03 13:45:52'),
(8, 'team_work', '#', 'أعضاء هيئة التدريس', 'و هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم', 'banners/CRQySAGIJ7UcgyWSOyeyD1d849Q0wuGTLs4xtgCs.jpeg', NULL, '2021-04-03 13:46:50', '2021-04-11 20:49:11'),
(9, 'courses', '#', 'كورسات ودورات مفتوحة', 'و هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم', 'banners/elEG2MqBM5SgDTdyLkk1IgQI2NXLgMNz1GMvsOeK.jpeg', NULL, '2021-04-03 13:48:23', '2021-04-11 20:12:19'),
(10, 'how_to_study', '#', 'كيفيه الدراسة', 'و هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم', 'banners/rA2DGWEWfreJWQO0lon294bRDWfyEAEWHSoOQC8R.gif', NULL, '2021-04-03 13:49:30', '2021-04-03 13:49:30'),
(11, 'how_to_subscribe', '#', 'كيفيه الالتحاق', 'و هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم', 'banners/vMSNKcK6Ijiv7fD92UwkGFi0EKpOhKwaQHdvQZF1.gif', NULL, '2021-04-03 13:50:37', '2021-04-03 13:50:37'),
(12, 'news', '#', 'الاخبار', 'و هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم', 'banners/C5Go3MvYhb0f7mshCjmZpRbBuiwJSnsUCVpwnADO.jpeg', NULL, '2021-04-03 13:51:24', '2021-04-11 21:39:17'),
(13, 'activities', '#', 'الانشطه', 'و هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم', 'banners/buvJF1N7U0rESx43pB5S562lmFm5gQv5aperwdpo.jpeg', NULL, '2021-04-03 13:51:57', '2021-04-03 13:51:57'),
(15, 'contact_us', '#', 'تواصل معنا', 'بتسجيلك فى برنامج الأكاديمية الوطنية للعلوم الشاملة تتجاوز العقبات وتتعلم العلم بكل يسر وسهولة من خلال الوسائل المتقدمة المعتمدة فى برنامج الأكاديمية ...', 'banners/QT0daOUNTIcnwX7xyvoVh9UYBN9cy9vYFWwHkl5C.jpeg', NULL, '2021-04-11 21:51:39', '2021-04-11 21:51:39');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, NULL, 'clients/gJGiQKauO94unWX5P41ozOBmJ93HUxUzyiFgzi7M.jpeg', '2021-04-14 14:08:53', '2021-04-14 14:11:53'),
(4, NULL, 'clients/1golH04AajdpdoeEGDM3xGWdIovH5lkbBkZSSoJB.jpeg', '2021-04-14 14:12:02', '2021-04-14 14:12:02'),
(5, NULL, 'clients/cCFmbUi9MKyBlw317ro36EZAmsRLfeOcHBZeG7Oj.jpeg', '2021-04-14 14:12:10', '2021-04-14 14:12:10');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` enum('read','unread') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `message`, `phone`, `subject`, `is_read`, `created_at`, `updated_at`) VALUES
(1, 'mostafa', 'm@m.com', 'موقع جديد للأكاديمية الوطنية للعلوم الشاملة', NULL, NULL, NULL, '2021-04-11 22:06:05', '2021-04-11 22:06:05');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `level_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contents` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ليس ضرورى',
  `price` double NOT NULL DEFAULT 0,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `level_id`, `title`, `contents`, `price`, `image`, `created_at`, `updated_at`) VALUES
(1, 1, 'النطق', 'الرِّيادةُ في التعليم الشرعي الافتراضي المفتوح بأحدث الطرق والوسائل المقترن بالبث الفضائي على قناة زاد وشبكة الإنترنت. والسعي لتقريب العلم الصحيح من خلال صياغة وتقديم مناهج شرعية موثوقة كتبت بلغة العصر بمعايير لغوية وشرعية عالية مع مراعاة التصميم التعليمي الاحترافي.لتعليم الافتراضي عن بعد من خلال توفير منهج علمي يقدم للطالب أساسيات العلم الشرعي وما لا يسع طالب العلم جهله مع مراعاة الاحترافية والابتكار في أساليب العرض. توضيح وتقريب مذهب أهل السنة والجماعة في المعتقد، ونشر الفقه الصحيح بدليله من الكتاب والسنة تعليم المرأة المسلمة دينها، وتوعيتها وتثقيفها، والمساهمة بإعداد طالبات علم شرعي.', 30, 'courses/2PquJIHnFnA4n0xwzYruaSTAnBHJZFpMsa9U0kYb.jpeg', '2021-03-31 20:53:30', '2021-04-13 22:11:36'),
(3, 2, 'كورس المحادثة', 'الرِّيادةُ في التعليم الشرعي الافتراضي المفتوح بأحدث الطرق والوسائل المقترن بالبث الفضائي على قناة زاد وشبكة الإنترنت. والسعي لتقريب العلم الصحيح من خلال صياغة وتقديم مناهج شرعية موثوقة كتبت بلغة العصر بمعايير لغوية وشرعية عالية مع مراعاة التصميم التعليمي الاحترافي.لتعليم الافتراضي عن بعد من خلال توفير منهج علمي يقدم للطالب أساسيات العلم الشرعي وما لا يسع طالب العلم جهله مع مراعاة الاحترافية والابتكار في أساليب العرض.', 40, 'courses/PmQzqvnjZKVA4FJ42MEiVB8MC7w0xGcr6USyOV4y.jpeg', '2021-03-31 21:05:12', '2021-04-13 21:47:00'),
(7, 1, 'القراءه', 'الرِّيادةُ في التعليم الشرعي الافتراضي المفتوح بأحدث الطرق والوسائل المقترن بالبث الفضائي على قناة زاد وشبكة الإنترنت.  والسعي لتقريب العلم الصحيح من خلال صياغة وتقديم مناهج شرعية موثوقة كتبت بلغة العصر بمعايير لغوية وشرعية عالية مع مراعاة التصميم التعليمي الاحترافي.لتعليم الافتراضي عن بعد من خلال توفير منهج علمي يقدم للطالب أساسيات العلم الشرعي وما لا يسع طالب العلم جهله مع مراعاة الاحترافية والابتكار في أساليب العرض.  توضيح وتقريب مذهب أهل السنة والجماعة في المعتقد، ونشر الفقه الصحيح بدليله من الكتاب والسنة  تعليم المرأة المسلمة دينها، وتوعيتها وتثقيفها، والمساهمة بإعداد طالبات علم شرعي.', 300, 'courses/DiOg7Wo7tpOhIbCdMU6mwYhiZrCZNySdkKiqMqOU.jpeg', '2021-04-13 22:06:02', '2021-04-13 22:12:02');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` enum('activities') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `related_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `type`, `related_id`, `image`, `created_at`, `updated_at`) VALUES
(4, 'activities', 6, 'activitys/6/mbB9TIuMw2KDsd64Rpf0IytKZHiH3DTHavmVNblv.jpeg', '2021-04-03 00:39:06', '2021-04-03 00:39:06'),
(5, 'activities', 6, 'activitys/6/dQTkcz299H0OhbVUdSPp1GlLeIYqksuuoq36Gu2H.jpeg', '2021-04-03 13:21:06', '2021-04-03 13:21:06'),
(9, 'activities', 7, 'activitys/7/GBd1eqUBNwFf0UvU1H6PZeUBqLbIieA5SWt1dAK4.jpeg', '2021-04-03 13:23:50', '2021-04-03 13:23:50'),
(10, 'activities', 1, 'activitys/1/EQqS9Tcu6Yauu4sHYCRthVnawEte0Xh07FGoH2Xt.jpeg', '2021-04-03 13:24:18', '2021-04-03 13:24:18'),
(11, 'activities', 1, 'activitys/1/OBldkGHdtvEAluBd3JCu7g0ZBffiINOzIGEpKZa8.gif', '2021-04-03 13:24:18', '2021-04-03 13:24:18'),
(12, 'activities', 1, 'activitys/1/tCzNJOgsnMxeUJ9g4OVPSvn4VAxYFvZJzTm7pqcJ.jpeg', '2021-04-03 13:24:18', '2021-04-03 13:24:18'),
(13, 'activities', 1, 'activitys/1/60biCvSm7OxD7yrM5BTLlKr0hAOojRjy5wmCjy8s.gif', '2021-04-03 13:24:18', '2021-04-03 13:24:18'),
(14, 'activities', 1, 'activitys/1/oq1eaA5kL3RsT8Lxuz3HlDcBlIl6LgY7knNUHfqd.jpeg', '2021-04-03 13:24:18', '2021-04-03 13:24:18');

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contents` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ليس ضرورى',
  `price` double NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `title`, `contents`, `price`, `created_at`, `updated_at`) VALUES
(1, 'المستوى الاول', NULL, 50, '2021-03-31 20:45:46', '2021-03-31 20:46:02'),
(2, 'المستوى الثانى', NULL, 100, '2021-03-31 20:46:16', '2021-03-31 20:46:16');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(7, '2019_07_31_101142_create_visits_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_04_09_105520_create_settings_table', 1),
(10, '2020_04_09_105529_create_admins_table', 1),
(11, '2020_04_19_161046_create_site_texts_table', 1),
(12, '2020_04_19_163423_create_contacts_table', 1),
(13, '2020_10_12_000000_create_users_table', 1),
(14, '2021_03_29_223518_create_banners_table', 1),
(15, '2021_03_29_223658_create_teams_table', 1),
(16, '2021_03_29_223923_create_levels_table', 1),
(17, '2021_03_29_224255_create_courses_table', 1),
(18, '2021_03_29_224647_create_open_courses_table', 1),
(19, '2021_03_29_225007_create_activity_departments_table', 1),
(20, '2021_03_29_225127_create_activities_table', 1),
(21, '2021_03_29_230222_create_images_table', 1),
(22, '2021_03_29_230459_create_news_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contents` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `contents`, `image`, `created_at`, `updated_at`) VALUES
(5, 'موقع جديد للأكاديمية الوطنية للعلوم الشاملة', 'تم انتقال أكاديمية الوطنية للتدريب الى مقرة الجديد فى القاهرة مدينة نصر .', 'courses/AbrypGwaHS9dL9PlU29LSBghx0qvso0t698br3AD.jpeg', '2021-04-11 21:46:05', '2021-04-11 21:46:05'),
(6, 'موقع جديد للأكاديمية الوطنية للعلوم الشاملة 22', 'تم انتقال أكاديمية الى مقرة الجديد فى القاهرة مدينة نصر .', 'courses/INXNcuy8OVP1h0qnV2TOFJfxMSZIBH0vV9fB11lf.jpeg', '2021-04-11 21:47:26', '2021-04-11 21:47:26');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `open_courses`
--

CREATE TABLE `open_courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contents` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double NOT NULL DEFAULT 0 COMMENT 'ليس ضرورى',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `open_courses`
--

INSERT INTO `open_courses` (`id`, `title`, `contents`, `image`, `duration`, `price`, `created_at`, `updated_at`) VALUES
(1, 'كورس جديد شامل', 'لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم مطبوعه ... بروشور او فلاير على سبيل المثال ... او نماذج مواقع انترنت', 'courses/8QpgSFnrTYcwHvNwTetjzRdeEPpn7Eqxv2OwVmOJ.jpeg', '4 شهور', 0, '2021-03-31 21:17:56', '2021-03-31 21:46:20');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `registers`
--

CREATE TABLE `registers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `student_name` varchar(255) DEFAULT NULL,
  `qualification` varchar(255) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `course_id` bigint(20) UNSIGNED DEFAULT NULL,
  `activity_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `registers`
--

INSERT INTO `registers` (`id`, `student_name`, `qualification`, `age`, `address`, `phone`, `email`, `course_id`, `activity_id`, `created_at`, `updated_at`) VALUES
(7, 'مصطفى', 'حاسبات', '24', 'Fallujah, g', '01025130204', 'd2@d2.com', NULL, 6, '2021-04-13 23:09:51', '2021-04-13 23:09:51'),
(8, 'مصطفى 3', 'ليسانس حقوق', '24', 'Ramadi, Iraq', '01025130204', 'doc@doc.com', NULL, NULL, '2021-04-13 23:10:13', '2021-04-13 23:10:13'),
(9, 'احمد على 2', 'ليسانس حقوق', '24', 'Unnamed Road, Hasai, Iraq', '01025130204', 'd2@d2.com', 1, NULL, '2021-04-13 23:10:44', '2021-04-13 23:10:44'),
(10, 'مصطفى', 'حاسبات', '24', 'Unnamed Road, Hasai, Iraq', '01025130204', 'd2@d2.com', NULL, NULL, '2021-04-14 14:26:30', '2021-04-14 14:26:30'),
(11, 'مصطفى111', 'حاسبات', '24', 'Unnamed Road, Hasai, Iraq', '01025130204', 'd2@d2.com', NULL, NULL, '2021-04-14 14:26:40', '2021-04-14 14:26:40');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `header_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_banner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_slider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_desc` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_profile_pdf` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `android_app` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ios_app` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` double NOT NULL DEFAULT 0,
  `longitude` double NOT NULL DEFAULT 0,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `sms_user_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sms_user_pass` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sms_sender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publisher` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ar',
  `default_theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `offer_muted` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `offer_notification` int(11) NOT NULL DEFAULT 1,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telegram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_plus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `snapchat_ghost` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_app` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web_site_name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_termis_condition` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `en_termis_condition` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_commission` int(11) NOT NULL DEFAULT 1,
  `debt_limit` int(11) NOT NULL DEFAULT 1,
  `currency_from_eg_to_Em` double NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `header_logo`, `footer_logo`, `login_banner`, `image_slider`, `title`, `desc`, `footer_desc`, `company_profile_pdf`, `address1`, `address2`, `phone1`, `phone2`, `fax`, `android_app`, `ios_app`, `email1`, `email2`, `link`, `latitude`, `longitude`, `address`, `sms_user_name`, `sms_user_pass`, `sms_sender`, `publisher`, `default_language`, `default_theme`, `offer_muted`, `offer_notification`, `facebook`, `twitter`, `instagram`, `linkedin`, `telegram`, `youtube`, `google_plus`, `snapchat_ghost`, `whatsapp`, `about_app`, `web_site_name`, `ar_termis_condition`, `en_termis_condition`, `site_commission`, `debt_limit`, `currency_from_eg_to_Em`, `created_at`, `updated_at`) VALUES
(1, 'setting/wTAPGF1kSRiIAwZ01pehRvQWIbWpRxpeVJV65IR3.png', NULL, 'setting/myutRpyjgI57xwkWI9DmSJy8X044zmrYCGvm8eqL.jpeg', NULL, 'اكاديمى', NULL, NULL, NULL, 'القاهرة -مدينة نصر -شارع عباس العقاد .', '#', '01024578962', '01000066556', NULL, NULL, NULL, 'info@edu.com', NULL, NULL, 0, 0, '0', NULL, NULL, NULL, NULL, 'ar', NULL, NULL, 1, 'https://www.facebook.com', '#', NULL, NULL, NULL, 'https://www.youtube.com/', 'https://www.google.com', NULL, '0539044145', NULL, '#', NULL, NULL, 1, 1, 1, '2021-03-29 21:15:37', '2021-04-13 19:51:40');

-- --------------------------------------------------------

--
-- Table structure for table `site_texts`
--

CREATE TABLE `site_texts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key_word` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contents` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `site_texts`
--

INSERT INTO `site_texts` (`id`, `key_word`, `title`, `contents`, `logo`, `video`, `created_at`, `updated_at`) VALUES
(1, 'vision', 'الرؤيه', 'و هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم', 'site_texts/NCHz7LWS3p9QeZFw5g0fabz4GhEnKxc3Qz7N7wmG.jpeg', 'site_texts/m6dVcLjKtRo2lKrLLJUPWp6VddnVu4Z9QmozOpPA.mp4', '2021-04-03 15:41:58', '2021-04-03 15:41:58'),
(2, 'message', 'الرساله', 'و هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم', 'site_texts/s2ozySrFiH5Upm3XTk2i312cbDsuicoxuE5N0hK6.jpeg', 'site_texts/Tqtxx9Tk1UeaYoSkyIxLpXErLlF94C45BAdYhobl.mp4', '2021-04-03 15:44:12', '2021-04-03 15:44:12'),
(3, 'goals', 'الاهداف', 'و هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم', 'site_texts/1KVhj0vdMw4M4vgjPbqOcG8R22vq0tFPODgKUfvF.png', 'site_texts/SOyiBF3i9xs2KMg0ZSMNX8NMFpzmEQoUz19eAUjI.mp4', '2021-04-03 15:45:26', '2021-04-03 15:45:26'),
(4, 'word_of_prestent', 'كلمه رئيس الاكاديميه', 'و هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم', 'site_texts/BiC5LfOPsovcpCYWlDuW3zAMpBsRZzJrhPClRFXU.jpeg', 'site_texts/uKihrtoA2rRXxNBAXwtuU74ipDQqUD0vEXPzKkXx.mp4', '2021-04-03 15:46:28', '2021-04-03 15:46:28'),
(5, 'how_to_study', 'الدراسة بالاكاديمية', 'يطمح برنامج الأكاديمية للوصول إلى 500 ألف مستفيد من هذا البرنامج توفير مناهج خاصة بالبرنامج ومكتملة متوفرة إلكترونية ومتاحا ورقيا في المكتبات تخريج دفعتين خلال هذه الخمس سنوات حتى عام 1442هـ.يطمح برنامج الأكاديمية للوصول إلى 500 ألف مستفيد من هذا البرنامج. توفير مناهج خاصة بالبرنامج ومكتملة متوفرة إلكترونية ومتاحا ورقيا في المكتبات تخريج دفعتين خلال هذه الخمس سنوات حتى عام 1442ه توفير مناهج خاصة بالبرنامج ومكتملة متوفرة إلكترونية ومتاحا ورقيا في المكتبات تخريج دفعتين خلال هذه الخمس سنوات حتى عام 1442هـ.يطمح برنامج الأكاديمية للوصول إلى 500 ألف مستفيد من هذا البرنامج. توفير مناهج خاصة بالبرنامج ومكتملة متوفرة إلكترونية ومتاحا ورقيا في المكتبات تخريج دفعتين خلال هذه الخمس سنوات حتى عام 1442ه', 'site_texts/UonHPJOzbI3qYxv0tzO1OuLr1YsM15CPtH6it5VI.jpeg', 'site_texts/Nm5ZLgdTquen8yl236eF6J1aGT3SAN3PFRK1vBdr.mp4', '2021-04-03 15:47:14', '2021-04-11 21:16:15'),
(6, 'how_to_subscribe', 'الالتحاق بالاكاديمية', 'الرِّيادةُ في التعليم الشرعي الافتراضي المفتوح بأحدث الطرق والوسائل المقترن بالبث الفضائي على قناة زاد وشبكة الإنترنت. والسعي لتقريب العلم الصحيح من خلال صياغة وتقديم مناهج شرعية موثوقة كتبت بلغة العصر بمعايير لغوية وشرعية عالية مع مراعاة التصميم التعليمي الاحترافي.لتعليم الافتراضي عن بعد من خلال توفير منهج علمي يقدم للطالب أساسيات العلم الشرعي وما لا يسع طالب العلم جهله مع مراعاة الاحترافية والابتكار في أساليب العرض. توضيح وتقريب مذهب أهل السنة والجماعة في المعتقد، ونشر الفقه الصحيح بدليله من الكتاب والسنة تعليم المرأة المسلمة دينها، وتوعيتها وتثقيفها، والمساهمة بإعداد طالبات علم شرعي . إفادة الجالية المسلمة في الغرب ببرنامج تعليمي يقرب إليهم ما يحتاجون إليه من العلم الشرعي الذي يحتاجون إليه لإقامة دينهم وتوسيع دائرة الاستفادة منه للمسلمين في مختلف أنحاء العالم.', 'site_texts/3PV9Fxe2aZjhG8BQxUcb1t4xb8jLmiEMmIcDQJSt.jpeg', 'site_texts/6QFvUVjtScxeDrEIRL8MFoMaPxz5TKJgfr9ZtzS7.mp4', '2021-04-03 15:47:53', '2021-04-11 21:15:57'),
(7, 'main_about', 'الكلمه فى الرئيسيه', 'و هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم', 'site_texts/o6j1ckPtCClS7YOAJO9xeoBls3Aye65mvabgPCPA.jpeg', 'site_texts/TvnwPkapUJ5arzzwyFOtMYyUJb55mWh1AMz3xY9t.mp4', '2021-04-03 15:48:57', '2021-04-03 15:48:57'),
(8, 'legal_counsel', 'المستشار القانونى', 'و هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم', 'site_texts/Cw1wVgluTQXU46D2tQHAe4IHvxMJRDWkQrdhgnxY.jpeg', 'site_texts/T7hEsdmW8gVLtxR9kqlIlcHrgY9L5p0qlECLj24v.mp4', '2021-04-03 15:49:40', '2021-04-03 15:49:40');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contents` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cv` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `face_book` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `position`, `contents`, `cv`, `face_book`, `twitter`, `instagram`, `created_at`, `updated_at`) VALUES
(1, 'احمد محمد ', 'دكتور جامعى', 'دكتوراة فى العلوم والدراسات وحاصل على شهادة الدكتوراة جامعة المنوفية ....', 'teams/4VpwBVZYBFZ6Vt0N2h4ikiX0ZyPIece8ApgSGQlo.jpeg', 'https://www.google.com', '#', '#', '2021-03-31 21:42:05', '2021-04-10 20:52:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` enum('student','teacher') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'student',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` double NOT NULL DEFAULT 0,
  `longitude` double NOT NULL DEFAULT 0,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `is_blocked` enum('blocked','not_blocked') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'not_blocked',
  `is_login` enum('online','offline') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'offline',
  `logout_time` int(11) DEFAULT NULL,
  `is_confirmed` int(11) NOT NULL DEFAULT 0,
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forget_password_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `software_type` enum('ios','android','web') COLLATE utf8mb4_unicode_ci DEFAULT 'web',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `visits`
--

CREATE TABLE `visits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `count` int(11) NOT NULL DEFAULT 0,
  `date` date DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activities_activity_department_id_foreign` (`activity_department_id`);

--
-- Indexes for table `activity_departments`
--
ALTER TABLE `activity_departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `courses_level_id_foreign` (`level_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `open_courses`
--
ALTER TABLE `open_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `registers`
--
ALTER TABLE `registers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_id` (`course_id`),
  ADD KEY `activity_id` (`activity_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_texts`
--
ALTER TABLE `site_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `visits`
--
ALTER TABLE `visits`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `activity_departments`
--
ALTER TABLE `activity_departments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `open_courses`
--
ALTER TABLE `open_courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `registers`
--
ALTER TABLE `registers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `site_texts`
--
ALTER TABLE `site_texts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `visits`
--
ALTER TABLE `visits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activities`
--
ALTER TABLE `activities`
  ADD CONSTRAINT `activities_activity_department_id_foreign` FOREIGN KEY (`activity_department_id`) REFERENCES `activity_departments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_level_id_foreign` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `registers`
--
ALTER TABLE `registers`
  ADD CONSTRAINT `registers_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `registers_ibfk_2` FOREIGN KEY (`activity_id`) REFERENCES `activities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
